#!/bin/bash
wget https://repo.percona.com/apt/percona-release_0.1-4.$(lsb_release -sc)_all.deb && \
dpkg -i percona-release_0.1-4.$(lsb_release -sc)_all.deb && \
debconf-set-selections <<< 'percona-server-server-5.7 percona-server-server-5.7/root-pass password root'
debconf-set-selections <<< 'percona-server-server-5.7 percona-server-server-5.7/re-root-pass password root'
debconf-set-selections <<< 'phpmyadmin phpmyadmin/dbconfig-install boolean true'
debconf-set-selections <<< 'phpmyadmin phpmyadmin/app-password-confirm password root'
debconf-set-selections <<< 'phpmyadmin phpmyadmin/mysql/admin-pass password root'
debconf-set-selections <<< 'phpmyadmin phpmyadmin/mysql/app-pass password root'
debconf-set-selections <<< 'phpmyadmin phpmyadmin/reconfigure-webserver multiselect apache2'﻿
apt update  && apt -y install percona-server-server-5.7 apache2 phpmyadmin
