#!/bin/bash
function replace {
if [ ! -f "$1" ]; then
  echo "File ${1} does not exist"
  exit 1
fi
if [ -z "$2" ];then
  echo -e "You need to set output file.For example:\n./template.sh file1 file2"
  exit 1
fi
TEMPLATE_FILE="$1"
OUTPUT_FILE="$2"
cp $TEMPLATE_FILE $OUTPUT_FILE
var_arrays=$(grep '{\$[[:space:]].*[[:space:]]\$}' "$TEMPLATE_FILE" | awk '{print $2}')
for item in $var_arrays;do
  local subs_flag=false
  for env_var in $(env);do
    value=${env_var/*=/}
    key=${env_var/\=*/}
    if [[ $item == $key ]];then
        sed -i 's!{\$.'"$key"'.\$}!'"$value"'!' $OUTPUT_FILE
        subs_flag=true
        break
    fi
  done
  if [[ $subs_flag != "true" ]];then
      sed -i 's!{\$.'"$item"'.\$}!\"\"!' $OUTPUT_FILE
      continue
  fi
done
}

replace $1 $2
